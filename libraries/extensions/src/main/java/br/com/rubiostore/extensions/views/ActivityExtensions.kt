package br.com.rubiostore.extensions.views

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

inline fun <reified T> AppCompatActivity.observe(
    liveData: LiveData<T>,
    noinline lifecycle: (T) -> Unit
) {
    liveData.observe(this, Observer(lifecycle))
}

inline fun <reified T> Activity.extra(key: String): Lazy<T> = lazy {
    val value = intent.extras?.get(key)
    if (value is T) value else throw IllegalArgumentException("didn't find extra with key $key")
}

fun AppCompatActivity.onBackButtonPressed() {
    onBackPressedDispatcher.onBackPressed()
}