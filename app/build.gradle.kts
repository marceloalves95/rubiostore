plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("jacoco-reports")
    id("io.gitlab.arturbosch.detekt")
}

android {
    namespace = "br.com.rubiostore"
    compileSdk = 34

    defaultConfig {
        applicationId = "br.com.rubiostore"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
            merges += "META-INF/LICENSE.md"
            merges += "META-INF/LICENSE-notice.md"
        }
    }
    tasks.withType<Test> {
        extensions.configure(JacocoTaskExtension::class) {
            isIncludeNoLocationClasses = true
            excludes = listOf("jdk.internal.*")
        }
    }
}

dependencies {

    implementation(project(":libraries:network"))
    implementation(project(":libraries:extensions"))
    implementation(project(":libraries:testing"))
    implementation(project(":libraries:compose"))
    implementation(libs.navigation.compose)

    //Debug Compose
    debugImplementation(libs.ui.tooling)
    debugImplementation(libs.ui.test.manifest)

    //Material
    implementation(libs.material)

    //AndroidX
    implementation(libs.bundles.androidx)

    // Constraint Layout Compose
    implementation(libs.constraintlayout.compose)

    //Compose
    implementation(libs.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.ui)
    implementation(libs.ui.graphics)
    implementation(libs.ui.tooling.preview)
    implementation(libs.runtime.livedata.compose)
    implementation(platform(libs.kotlin.bom))

    //Material3
    implementation(libs.material3)
    //Icons Material 3
    implementation(libs.material.icons.extended)

    //SplashScreen
    implementation(libs.splashscreen)

    //Lifecycle
    implementation(libs.bundles.lifecycle)

    //ThirdParty
    implementation(libs.koin)
    implementation(libs.coil)
    implementation(libs.bundles.retrofit)

    //Ktor
    implementation(libs.bundles.ktor)
    implementation(libs.slf4j.android)
    implementation(libs.kotlinx.serialization.json)

    //Unit Test
    testImplementation(libs.junit)
    testImplementation(libs.assertK)
    testImplementation(libs.mockk)
    testImplementation(libs.koin.test)
    testImplementation(libs.mockwebserver)

    //Instrumental Test
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.ui.test.junit4)
    androidTestImplementation(libs.test.ext.junit)
    androidTestImplementation(libs.espresso)
    androidTestImplementation(libs.assertK)
    androidTestImplementation(libs.test.core.ktx)
    androidTestImplementation(libs.hamcrest)
    androidTestImplementation(libs.arch.core.testing)
    androidTestImplementation(libs.barista)
    androidTestImplementation(libs.espresso)
}