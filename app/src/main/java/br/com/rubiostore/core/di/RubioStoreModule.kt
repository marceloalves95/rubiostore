package br.com.rubiostore.core.di

import br.com.rubiostore.data.RubioStoreRepositoryImpl
import br.com.rubiostore.data.remote.DataSourceRemoteImpl
import br.com.rubiostore.data.remote.service.RubioStoreService
import br.com.rubiostore.data.remote.service.RubioStoreServiceImpl
import br.com.rubiostore.domain.repository.RubioStoreRepository
import br.com.rubiostore.domain.usecase.remote.GetListAllProductsUseCase
import br.com.rubiostore.domain.usecase.remote.PostLoginUseCase
import br.com.rubiostore.network.di.NetworkModule.networkModule
import br.com.rubiostore.presentation.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module

object RubioStoreModule {

    fun load() {
        loadKoinModules(
            listOf(
                networkModule(),
                dataModule(),
                domainModule(),
                presentationModule()
            )
        )
    }

    private fun dataModule(): Module = module {
//        factory<RubioStoreApi> {
//            Service.createService(
//                baseUrl = BASE_URL
//            )
//        }
        single<RubioStoreService> {
            RubioStoreServiceImpl(get())
        }
        //single { DataSourceLocalImpl() }
        single { DataSourceRemoteImpl(get()) }
        single<RubioStoreRepository> {
            RubioStoreRepositoryImpl(
                //dataSourceLocal = get(),
                dataSourceRemote = get()
            )
        }
    }

    private fun domainModule(): Module = module {
        factory {
            GetListAllProductsUseCase(get())
        }
        factory {
            PostLoginUseCase(get())
        }
    }

    private fun presentationModule(): Module = module {
        viewModel {
            LoginViewModel(get())
        }
    }
}
