package br.com.rubiostore.core

import android.app.Application
import br.com.rubiostore.core.di.RubioStoreModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Application)
        }
        RubioStoreModule.load()
    }
}
