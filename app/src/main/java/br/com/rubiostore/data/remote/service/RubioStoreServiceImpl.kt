package br.com.rubiostore.data.remote.service

import br.com.rubiostore.data.BASE_URL
import br.com.rubiostore.data.remote.models.LoginResponse
import br.com.rubiostore.data.remote.models.ProductsResponse
import br.com.rubiostore.domain.models.LoginBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType

class RubioStoreServiceImpl(
    private val httpClient: HttpClient
) : RubioStoreService {
    override suspend fun listAllProducts(): List<ProductsResponse> {
        return httpClient.get(BASE_URL).body()
    }

    override suspend fun login(body: LoginBody): LoginResponse {
        return httpClient.post("https://fakestoreapi.com/auth/login"){
            contentType(ContentType.Application.Json)
            setBody(body)
        }.body()
    }

}