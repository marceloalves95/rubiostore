package br.com.rubiostore.data.remote

import br.com.rubiostore.data.remote.service.RubioStoreService
import br.com.rubiostore.data.remote.mapper.toLogin
import br.com.rubiostore.data.remote.mapper.toProducts
import br.com.rubiostore.data.source.DataSource
import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.models.Products

class DataSourceRemoteImpl(
    private val service: RubioStoreService
) : DataSource.Remote {
    override suspend fun listAllProducts(): List<Products> {
        return service.listAllProducts().map { it.toProducts() }
    }

    override suspend fun login(body: LoginBody): Login {
        return service.login(body).toLogin()
    }
}
