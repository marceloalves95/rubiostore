package br.com.rubiostore.data.remote.mapper

import br.com.rubiostore.data.remote.models.LoginResponse
import br.com.rubiostore.data.remote.models.ProductsRatingResponse
import br.com.rubiostore.data.remote.models.ProductsResponse
import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.models.Products
import br.com.rubiostore.domain.models.ProductsRating

internal fun ProductsResponse.toProducts() = Products(
    id = id,
    title = title,
    price = price,
    description = description,
    category = category,
    image = image,
    rating = rating?.toProductsRating()
)

internal fun ProductsRatingResponse.toProductsRating() = ProductsRating(
    rate = rate,
    count = count
)

internal fun LoginResponse.toLogin() = Login(
    token = token
)
