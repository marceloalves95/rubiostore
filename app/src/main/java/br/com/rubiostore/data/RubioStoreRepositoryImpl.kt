package br.com.rubiostore.data

//import br.com.rubiostore.data.local.DataSourceLocalImpl
import br.com.rubiostore.data.remote.DataSourceRemoteImpl
import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.models.Products
import br.com.rubiostore.domain.repository.RubioStoreRepository

class RubioStoreRepositoryImpl(
    //private val dataSourceLocal: DataSourceLocalImpl,
    private val dataSourceRemote: DataSourceRemoteImpl
) : RubioStoreRepository {

    override suspend fun listAllProducts(): List<Products> {
        return dataSourceRemote.listAllProducts()
    }

    override suspend fun login(body: LoginBody): Login {
        return dataSourceRemote.login(body)
    }

}
