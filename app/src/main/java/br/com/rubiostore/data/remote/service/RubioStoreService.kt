package br.com.rubiostore.data.remote.service

import br.com.rubiostore.data.remote.models.LoginResponse
import br.com.rubiostore.data.remote.models.ProductsResponse
import br.com.rubiostore.domain.models.LoginBody

interface RubioStoreService {
    suspend fun listAllProducts():List<ProductsResponse>
    suspend fun login(body: LoginBody): LoginResponse
}