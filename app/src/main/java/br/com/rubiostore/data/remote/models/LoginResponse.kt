package br.com.rubiostore.data.remote.models

import kotlinx.serialization.Serializable

@Serializable
data class LoginResponse(
    val token: String?
)
