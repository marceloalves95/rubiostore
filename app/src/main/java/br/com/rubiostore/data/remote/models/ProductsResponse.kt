package br.com.rubiostore.data.remote.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ProductsResponse(
    @SerialName("id")
    val id: Int?,
    @SerialName("title")
    val title: String?,
    @SerialName("price")
    val price: Double?,
    @SerialName("description")
    val description: String?,
    @SerialName("category")
    val category: String?,
    @SerialName("image")
    val image: String?,
    @SerialName("rating")
    val rating: ProductsRatingResponse?
)

@Serializable
data class ProductsRatingResponse(
    @SerialName("rate")
    val rate: Double?,
    @SerialName("count")
    val count: Int?
)
