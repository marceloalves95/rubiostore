package br.com.rubiostore.data.source

import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.models.Products

sealed interface DataSource {

    //interface Local : DataSource {}

    interface Remote : DataSource {
        suspend fun listAllProducts(): List<Products>
        suspend fun login(body: LoginBody): Login
    }
}
