package br.com.rubiostore.domain.repository

import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.models.Products

interface RubioStoreRepository {
    suspend fun listAllProducts(): List<Products>
    suspend fun login(body: LoginBody): Login
}
