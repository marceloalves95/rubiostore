package br.com.rubiostore.domain.models

import kotlinx.serialization.Serializable

@Serializable
data class Login(
    val token: String?
)

@Serializable
data class LoginBody(
    val username: String?,
    val password: String?
)
