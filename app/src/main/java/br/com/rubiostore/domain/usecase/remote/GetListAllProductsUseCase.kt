package br.com.rubiostore.domain.usecase.remote

import br.com.rubiostore.domain.repository.RubioStoreRepository
import br.com.rubiostore.extensions.others.executeFlow

class GetListAllProductsUseCase(
    private val repository: RubioStoreRepository
) {
    suspend operator fun invoke() = executeFlow(
        getRepository = {
            repository.listAllProducts()
        }
    )
}
