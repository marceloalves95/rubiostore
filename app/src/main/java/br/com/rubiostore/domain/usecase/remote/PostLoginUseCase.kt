package br.com.rubiostore.domain.usecase.remote

import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.repository.RubioStoreRepository
import br.com.rubiostore.extensions.others.executeFlow

class PostLoginUseCase(
    private val repository: RubioStoreRepository
) {
    suspend operator fun invoke(body: LoginBody) = executeFlow(
        getRepository = {
            repository.login(body)
        }
    )
}
