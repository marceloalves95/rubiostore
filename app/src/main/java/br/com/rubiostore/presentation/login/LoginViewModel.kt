package br.com.rubiostore.presentation.login

import androidx.lifecycle.ViewModel
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.domain.usecase.remote.PostLoginUseCase
import br.com.rubiostore.extensions.others.launch
import br.com.rubiostore.network.event.Event
import br.com.rubiostore.presentation.login.model.RubioStoreState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class LoginViewModel(
    private val postLoginUseCase: PostLoginUseCase
) : ViewModel() {

    private val _state = MutableStateFlow<RubioStoreState>(RubioStoreState.Loading)
    val state: StateFlow<RubioStoreState> get() = _state

    fun login(loginBody: LoginBody) = launch {
        postLoginUseCase.invoke(loginBody).collect { event ->
            when (event) {
                is Event.Data -> {
                    _state.value = RubioStoreState.ScreenData(event.data)
                }

                is Event.Error -> {
                    _state.value = RubioStoreState.Error(event.error)
                }

                else -> Unit
            }
        }
    }
}
