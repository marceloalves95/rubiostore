package br.com.rubiostore.presentation.login

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import br.com.rubiostore.R
import br.com.rubiostore.compose.components.ScaffoldApp
import br.com.rubiostore.compose.others.UIModePreviews
import br.com.rubiostore.compose.theme.ThemeApp
import br.com.rubiostore.domain.models.LoginBody
import br.com.rubiostore.presentation.login.model.RubioStoreState
import br.com.rubiostore.presentation.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

@SuppressWarnings("FunctionNaming","MaxLineLength", "MagicNumber", "LongMethod")
class LoginActivity : ComponentActivity() {

    private lateinit var context: Context
    private val viewModel: LoginViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            context = LocalContext.current
            val state by viewModel.state.collectAsState()
            Screen()
            LoadScreen(state = state)
        }
    }

    @Composable
    fun Screen() {
        ThemeApp {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                ScaffoldApp(
                    title = stringResource(id = R.string.app_name),
                    content = {
                        ConstraintLayout(
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight()
                        ) {
                            val (fieldUsername, fieldPassword, buttonSignUp) = createRefs()
                            val guideline = createGuidelineFromTop(0.4f)

                            var username by remember { mutableStateOf("") }

                            OutlinedTextField(value = username,
                                onValueChange = {
                                    username = it
                                },
                                modifier = Modifier
                                    .padding(start = 16.dp, top = 8.dp, end = 16.dp)
                                    .wrapContentHeight()
                                    .constrainAs(fieldUsername) {
                                        width = Dimension.fillToConstraints
                                        top.linkTo(guideline)
                                        start.linkTo(parent.start)
                                        end.linkTo(parent.end)
                                    },
                                label = { Text(text = stringResource(id = R.string.username)) },
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                                colors = OutlinedTextFieldDefaults.colors(
                                    cursorColor = Color.Black,
                                    focusedBorderColor = MaterialTheme.colorScheme.primary,
                                    unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                                ),
                                trailingIcon = {
                                    when {
                                        username.isNotEmpty() -> IconButton(onClick = {
                                            username = ""
                                        }) {
                                            Icon(
                                                imageVector = Icons.Filled.Clear,
                                                contentDescription = "Clear"
                                            )
                                        }
                                    }
                                })

                            var password by rememberSaveable { mutableStateOf("") }
                            var passwordVisible by rememberSaveable { mutableStateOf(false) }

                            OutlinedTextField(value = password,
                                onValueChange = { password = it },
                                modifier = Modifier
                                    .padding(start = 16.dp, top = 8.dp, end = 16.dp)
                                    .wrapContentHeight()
                                    .constrainAs(fieldPassword) {
                                        width = Dimension.fillToConstraints
                                        top.linkTo(fieldUsername.bottom)
                                        start.linkTo(parent.start)
                                        end.linkTo(parent.end)
                                    },
                                label = {
                                    Text(text = stringResource(id = R.string.password))
                                },
                                visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
                                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                                colors = OutlinedTextFieldDefaults.colors(
                                    focusedBorderColor = MaterialTheme.colorScheme.primary,
                                    unfocusedBorderColor = MaterialTheme.colorScheme.secondary,
                                ),
                                trailingIcon = {
                                    val image = if (passwordVisible)
                                        Icons.Filled.Visibility
                                    else Icons.Filled.VisibilityOff

                                    val description =
                                        if (passwordVisible) "Hide password" else "Show password"

                                    IconButton(onClick = { passwordVisible = !passwordVisible }) {
                                        Icon(imageVector = image, description)
                                    }
                                })

                            Button(
                                onClick = {
                                    val loginBody = LoginBody(
                                        username = username,
                                        password = password
                                    )
                                    viewModel.login(loginBody)
                                },
                                modifier = Modifier
                                    .wrapContentHeight()
                                    .padding(end = 16.dp, start = 16.dp, bottom = 16.dp)
                                    .constrainAs(buttonSignUp) {
                                        width = Dimension.fillToConstraints
                                        start.linkTo(parent.start)
                                        end.linkTo(parent.end)
                                        bottom.linkTo(parent.bottom)
                                    },
                                shape = CutCornerShape(5)
                            ) {
                                Text(
                                    text = stringResource(id = R.string.sign_up),
                                    Modifier.padding(start = 10.dp)
                                )
                            }
                        }
                    }
                )
            }
        }
    }

    @Composable
    fun LoadScreen(state: RubioStoreState) {
        when (state) {
            is RubioStoreState.ScreenData -> ShowMessageSuccess()
            is RubioStoreState.Error -> ShowMessageError(exception = state.exception)
            else -> Unit
        }
    }

    @Composable
    fun ShowMessageSuccess() {
        val intent = MainActivity.newInstance(context)
        startActivity(intent)
    }

    @Composable
    fun ShowMessageError(exception: Throwable?) {
        Toast.makeText(context, R.string.login_incorrect, Toast.LENGTH_LONG).show()
        println(exception?.message?.let { Log.e("Error", it) })
    }

    @UIModePreviews
    @Composable
    fun ScreenPreview() {
        Screen()
    }
}
