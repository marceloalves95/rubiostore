package br.com.rubiostore.presentation.login.model

import br.com.rubiostore.domain.models.Login

sealed class RubioStoreState {
    object Loading: RubioStoreState()
    data class ScreenData(val login: Login) : RubioStoreState()
    data class Error(val exception: Throwable? = null) : RubioStoreState()
}
