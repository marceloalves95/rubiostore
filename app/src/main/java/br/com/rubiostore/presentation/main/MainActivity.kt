package br.com.rubiostore.presentation.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import br.com.rubiostore.compose.others.UIModePreviews
import br.com.rubiostore.compose.theme.ThemeApp
import br.com.rubiostore.presentation.main.screens.FavoriteScreen
import br.com.rubiostore.presentation.main.screens.HomeScreen
import br.com.rubiostore.presentation.main.screens.MyCartScreen
import br.com.rubiostore.presentation.navigation.BottomNavItem

@SuppressWarnings("FunctionNaming")
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ThemeApp {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Screen()
                }
            }
        }
    }

    @Composable
    fun Screen() {
        val navController = rememberNavController()
        Scaffold(
            bottomBar = {
                BottomNavigation(navController = navController)
            },
            content = {
                Column(
                    modifier = Modifier
                        .padding(it)
                        .fillMaxSize()
                ) {
                    NavigationGraph(navController = navController)
                }
            }
        )
    }

    @Composable
    fun BottomNavigation(navController: NavController) {
        val items = listOf(
            BottomNavItem.Home,
            BottomNavItem.Favorite,
            BottomNavItem.MyCart
        )
        NavigationBar(
            contentColor = Color.Black
        ) {
            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentRoute = navBackStackEntry?.destination?.route
            items.forEach { item ->
                NavigationBarItem(
                    label = {
                        Text(
                            text = item.label,
                            fontSize = 10.sp
                        )
                    },
                    selected = currentRoute == item.route,
                    onClick = {
                        navController.navigate(item.route) {
                            navController.graph.startDestinationRoute?.let { route ->
                                popUpTo(route) {
                                    saveState = true
                                }
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },
                    icon = {
                        Icon(imageVector = item.icon, contentDescription = item.label)
                    })
            }
        }
    }

    @Composable
    fun NavigationGraph(navController: NavHostController) {
        NavHost(navController, startDestination = BottomNavItem.Home.label) {
            composable(BottomNavItem.Home.label) {
                HomeScreen()
            }
            composable(BottomNavItem.Favorite.label) {
                FavoriteScreen()
            }
            composable(BottomNavItem.MyCart.label) {
                MyCartScreen()
            }
        }
    }



    @UIModePreviews
    @Composable
    fun MainActivityPreview() {
        Screen()
    }


    companion object {
        fun newInstance(
            context: Context
        ): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}
