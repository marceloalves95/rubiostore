package br.com.rubiostore.data.model

import br.com.rubiostore.data.remote.models.LoginResponse
import br.com.rubiostore.domain.models.LoginBody

@Suppress("MaxLineLength")
val dummyLoginResponse = LoginResponse(
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXIiOiJtb3JfMjMxNCIsImlhdCI6MTcwMjU2NjQyNX0.2VsAd8hY7u2nH9wrLSmp6GyNE_UE3BYAP5st49UAT5o"
)

val dummyLoginBody = LoginBody(
    username = "mor_2314",
    password = "83r5^_"
)
