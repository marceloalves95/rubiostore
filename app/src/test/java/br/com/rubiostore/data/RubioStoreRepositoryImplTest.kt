package br.com.rubiostore.data

import assertk.assertThat
import assertk.assertions.isEqualTo
import br.com.rubiostore.data.model.dummyLoginBody
import br.com.rubiostore.data.model.dummyLoginResponse
import br.com.rubiostore.data.remote.DataSourceRemoteImpl
import br.com.rubiostore.data.remote.mapper.toLogin
import br.com.rubiostore.testing.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class RubioStoreRepositoryImplTest : BaseTest() {

//    @RelaxedMockK
//    private lateinit var dataSourceRemote: DataSourceRemoteImpl
//
//    @RelaxedMockK
//    private lateinit var repository: RubioStoreRepositoryImpl
//
//    @Before
//    fun setup() {
//        repository = RubioStoreRepositoryImpl(dataSourceRemote)
//    }
//
//    @Test
//    fun `should verify login as correct when the repository is called with success`() =
//        runBlocking {
//
//            //Arrange
//            coEvery {
//                dataSourceRemote.login(dummyLoginBody)
//            } returns dummyLoginResponse.toLogin()
//
//            //Act
//            val result = repository.login(dummyLoginBody)
//
//            //Assert
//            assertThat(result).isEqualTo(dummyLoginResponse.toLogin())
//
//            coVerify(exactly = 1) {
//                dataSourceRemote.login(dummyLoginBody)
//            }
//
//            confirmVerified(dataSourceRemote)
//        }
//
//    @Test(expected = Throwable::class)
//    fun `should verify login as correct when the repository is called with failure`() =
//        runBlocking {
//
//            //Arrange
//            val error = mockk<Throwable>(relaxed = true)
//
//            coEvery {
//                dataSourceRemote.login(dummyLoginBody)
//            } throws error
//
//            //Act
//            repository.login(dummyLoginBody)
//
//            //Assert
//            coVerify(exactly = 1) {
//                dataSourceRemote.login(dummyLoginBody)
//            }
//
//            confirmVerified(dataSourceRemote)
//        }

}
