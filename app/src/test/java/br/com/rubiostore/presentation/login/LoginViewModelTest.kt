package br.com.rubiostore.presentation.login

import androidx.lifecycle.Observer
import br.com.rubiostore.data.model.dummyLoginBody
import br.com.rubiostore.domain.model.dummyLogin
import br.com.rubiostore.domain.usecase.remote.PostLoginUseCase
import br.com.rubiostore.network.event.Event
import br.com.rubiostore.presentation.login.model.RubioStoreState
import br.com.rubiostore.testing.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class LoginViewModelTest : BaseTest() {

//    @RelaxedMockK
//    private lateinit var postLoginUseCase: PostLoginUseCase
//
//    @RelaxedMockK
//    private lateinit var stateObserver: Observer<RubioStoreState>
//
//    @RelaxedMockK
//    private lateinit var viewModel: LoginViewModel
//
//    @Before
//    fun setup() {
//        viewModel = LoginViewModel(postLoginUseCase)
//    }
//
//    @Test
//    fun `should return login as correct when it is called with success`() = runBlocking {
//
//        //Arrange
//        val state = RubioStoreState.ScreenData(dummyLogin)
//
//        coEvery {
//            postLoginUseCase.invoke(dummyLoginBody)
//        } returns flowOf(
//            Event.loading(isLoading = true),
//            Event.data(dummyLogin)
//        )
//
//        coEvery {
//            stateObserver.onChanged(state)
//        } returns Unit
//
//        //Act
//        viewModel.login(dummyLoginBody)
//
//        //Assert
//        coVerify(exactly = 1) {
//            postLoginUseCase.invoke(dummyLoginBody)
//        }
//
//        confirmVerified(postLoginUseCase)
//    }
//
//    @Test
//    fun `should return login as incorrect when it is called with failure`() = runBlocking {
//
//        //Arrange
//        val error = mockk<Throwable>(relaxed = true)
//        val state = RubioStoreState.Error(exception = error)
//
//        coEvery {
//            postLoginUseCase.invoke(dummyLoginBody)
//        } returns flowOf(
//            Event.loading(isLoading = true),
//            Event.error(error)
//        )
//
//        coEvery {
//            stateObserver.onChanged(state)
//        } returns Unit
//
//        //Act
//        viewModel.login(dummyLoginBody)
//
//        //Assert
//        coVerify(exactly = 1) {
//            postLoginUseCase.invoke(dummyLoginBody)
//        }
//
//        confirmVerified(postLoginUseCase)
//    }

}
