package br.com.rubiostore.domain.model

import br.com.rubiostore.domain.models.Login

@Suppress("MaxLineLength")
val dummyLogin = Login(
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXIiOiJtb3JfMjMxNCIsImlhdCI6MTcwMjU2NjQyNX0.2VsAd8hY7u2nH9wrLSmp6GyNE_UE3BYAP5st49UAT5o"
)
