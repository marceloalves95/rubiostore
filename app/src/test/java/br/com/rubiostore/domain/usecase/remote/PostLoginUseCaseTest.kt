package br.com.rubiostore.domain.usecase.remote

import assertk.assertThat
import assertk.assertions.isEqualTo
import br.com.rubiostore.data.model.dummyLoginBody
import br.com.rubiostore.domain.model.dummyLogin
import br.com.rubiostore.domain.models.Login
import br.com.rubiostore.domain.repository.RubioStoreRepository
import br.com.rubiostore.network.event.Event
import br.com.rubiostore.testing.BaseTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class PostLoginUseCaseTest : BaseTest() {

//    @RelaxedMockK
//    private lateinit var postLoginUseCase: PostLoginUseCase
//
//    @MockK
//    private lateinit var repository: RubioStoreRepository
//
//    @Before
//    fun setup() {
//        postLoginUseCase = PostLoginUseCase(repository)
//    }
//
//    @Test
//    fun `should post login as correct when the use case of login is called with success`() =
//        runBlocking {
//
//            //Arrange
//            val progressEmit: MutableList<Event<Login>> = mutableListOf()
//
//            coEvery {
//                repository.login(dummyLoginBody)
//            } returns dummyLogin
//
//            //Act
//            postLoginUseCase.invoke(dummyLoginBody).collect { event ->
//                progressEmit.add(event)
//            }
//
//            //Assert
//            assertThat(progressEmit).isEqualTo(
//                mutableListOf(
//                    Event.Loading,
//                    Event.Data(dummyLogin)
//                )
//            )
//
//            coVerify {
//                repository.login(dummyLoginBody)
//            }
//
//            confirmVerified(repository)
//        }
//
//    @Test(expected = Throwable::class)
//    fun `should post login as incorrect when the use case of login is called with failure`() =
//        runBlocking {
//
//            //Arrange
//            val progressEmit: MutableList<Event<Login>> = mutableListOf()
//            val error = mockk<Throwable>(relaxed = true)
//
//            coEvery {
//                repository.login(dummyLoginBody)
//            } throws error
//
//            //Act
//            postLoginUseCase.invoke(dummyLoginBody).collect { event ->
//                progressEmit.add(event)
//            }
//
//            //Assert
//            assertThat(progressEmit).isEqualTo(
//                mutableListOf(
//                    Event.Loading,
//                    Event.Error(error)
//                )
//            )
//
//            coVerify {
//                repository.login(dummyLoginBody)
//            }
//
//            confirmVerified(repository)
//        }
}
